const duplicates = [
  "sushi",
  "pizza",
  "burger",
  "potatoe",
  "pasta",
  "ice-cream",
  "pizza",
  "chicken",
  "onion rings",
  "pasta",
  "soda",
];
const removeDuplicates = (list) => {
  let res = [];
  for (const element of list) {
    if (!res.includes(element)) {
      res.push(element);
    }
  }
  return res;
};

const result = removeDuplicates(duplicates);
console.log("listado final", result);
