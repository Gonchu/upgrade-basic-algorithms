const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];
 

let repeatCounter = (element) => {
    let counter = {};
    for (let i = 0; i < element.length; i++) {
      if (element[i] in counter) {
        counter[element[i]]++;
      } else {
        counter[element[i]] = 1;
      }
    }
    return console.log(counter);
  }

  repeatCounter(counterWords);

  /* puede ser asi tb con forEach
  
  let result = {};
  element.forEach(key => {
    if (key in result) {
      result[key] = result[key] + 1;
    } else {
      result[key] = 1;
    }
  });
  
  return result;
};*/ 