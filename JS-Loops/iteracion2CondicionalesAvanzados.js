/*Comprueba en cada uno de los usuarios que tenga 
al menos dos trimestres aprobados y añade 
la propiedad isApproved a true o false en consecuencia. 
Una vez lo tengas compruébalo con un console.log.*/

const alumns = [
    {name: 'Pepe Viruela', 
    T1: false, 
    T2: false, 
    T3: true}, 
	{name: 'Lucia Aranda',
    T1: true,
    T2: false, 
    T3: true},
	{name: 'Juan Miranda', 
    T1: false, 
    T2: true, 
    T3: true},
	{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
	{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]

for (let i = 0; i < alumns.length; i++) {
    alumn = alumns[i];
    let aproved = 0;

aproved = alumn.T1 ? aproved + 1 : aproved;
aproved = alumn.T2 ? aproved + 1 : aproved;
aproved = alumn.T3 ? aproved + 1 : aproved;
alumn.isApproved = aproved >= 2 ? true : false;
}

console.log(alumns);
