/* 1.1 Basandote en el array siguiente, 
crea una lista ul > li dinámicamente en el html 
que imprima cada uno de los paises.*/

const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const ulList = document.createElement('ul');

for (const element of countries) {
    let liItem = document.createElement('li');
    liItem.textContent = element;
    ulList.appendChild(liItem);
}
document.body.appendChild(ulList);

console.log(countries);


/*1.2 Elimina el elemento que tenga la clase .fn-remove-me.*/

let deleteClass = document.querySelector('.fn-remove-me');
deleteClass.remove();

/*1.3 Utiliza el array para crear dinamicamente una lista ul > li 
de elementos en el div de html 
con el atributo data-function="printHere".*/

const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

const divContainer = document.querySelector('[data-function="printHere"]');
const carList = document.createElement('ul');
divContainer.appendChild(carList);

for (const element of cars) {
    let liItem = document.createElement('li');
    liItem.textContent = element;
    carList.appendChild(liItem);
}

/*1.4 Crea dinamicamente en el html una lista de div 
que contenga un elemento h4 para el titulo 
y otro elemento img para la imagen.*/

const countriees = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

for (const element of countriees) {
    const listDiv = document.createElement('div');
    listDiv.innerHTML = `<h4>${element.title}</h4><img src=${element.imgUrl} />`;

    document.body.appendChild(listDiv);
}

/*1.5 Basandote en el ejercicio anterior. 
Crea un botón que elimine el último elemento de la lista.*/




/*1.6 Basandote en el ejercicio anterior. 
Crea un botón para cada uno de los elementos de las listas 
que elimine ese mismo elemento del html.*/