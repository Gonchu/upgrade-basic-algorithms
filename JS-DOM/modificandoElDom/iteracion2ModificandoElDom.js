/*
2.1 Inserta dinamicamente en un html un div vacio con javascript.
*/

const newDiv = document.createElement('div');
document.body.appendChild(newDiv);


/*2.2 Inserta dinamicamente en un html 
un div que contenga una p con javascript.*/

const newDivWhitP = document.createElement('div');
const newP = document.createElement('p');
newDivWhitP.appendChild(newP);
document.body.appendChild(newDivWhitP);


/*2.3 Inserta dinamicamente en un html 
un div que contenga 6 p utilizando un loop con javascript.*/

const otherDiv = document.createElement('div');
document.body.appendChild(otherDiv);

for (let i = 0; i < 6; i++) {
    let paragraph = document.createElement('p');
    otherDiv.appendChild(paragraph);
}

/*2.4 Inserta dinamicamente con javascript en un html
una p con el texto 'Soy dinámico!'.*/

const otherP = document.createElement('p');
otherP.textContent = 'Soy dinámico!';
document.body.appendChild(otherP);

/*2.5 Inserta en el h2 con la clase .fn-insert-here 
el texto 'Wubba Lubba dub dub'.*/

const h2Text = document.querySelector('.fn-insert-here');
h2Text.textContent = 'Wubba Lubba dub dub';

/*2.6 Basandote en el siguiente array crea una lista ul > li 
con los textos del array.*/

const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

let newList = document.createElement('ul');

for (const element of apps) {
    let li = document.createElement('li');
    li.textContent = element;
    newList.appendChild(li);
}

document.body.appendChild(newList);

/*2.7 Elimina todos los nodos que tengan 
la clase .fn-remove-me*/

let deleteElements = document.querySelectorAll(".fn-remove-me"); 
for (const element of deleteElements) {
    element.remove();
}

/*2.8 Inserta una p con el texto 'Voy en medio!' 
entre los dos div. 
Recuerda que no solo puedes insertar elementos 
con .appendChild.*/

let newParagraph = document.createElement('p');
selectDivs = document.querySelectorAll('div');

newParagraph.innerText = 'Voy en medio!';

document.body.insertBefore(newParagraph, selectDivs);

/*2.9 Inserta p con el texto 'Voy dentro!', 
dentro de todos los div con la clase .fn-insert-here*/

const selectClass = document.querySelectorAll('.fn-insert-here');
for (const element of selectClass) {
    let ootherP = document.createElement('p');
    ootherP.innerText = 'Voy dentro!';

    element.appendChild(ootherP);
}
