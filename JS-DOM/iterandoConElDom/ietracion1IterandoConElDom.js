/* 1.1 Usa querySelector para mostrar por consola 
el botón con la clase .showme */

showButton = document.querySelector(".showme");
console.log(showButton);


/* 1.2 Usa querySelector para mostrar por consola
el h1 con el id #pillado*/

h1Pillado = document.querySelector("#pillado");
console.log(h1Pillado);

/* 1.3 Usa querySelector para mostrar por consola
todos los p*/

paragraph = document.querySelectorAll("p");
console.log(paragraph);

/* 1.4 Usa querySelector para mostrar por consola 
todos los elementos con la clase.pokemon*/

pokemon = document.querySelectorAll(".pokemon");
console.log(pokemon);

/* 1.5 Usa querySelector para mostrar por consola 
todos los elementos con el atributo 
data-function="testMe".*/

dataFunction = document.querySelectorAll('[data-function="testMe"]');
console.log(dataFunction);

/* 1.6 Usa querySelector para mostrar por consola 
el 3 personaje con el atributo 
data-function="testMe".*/

personaje = document.querySelectorAll('[data-function="testMe"]');
console.log(personaje[2]);