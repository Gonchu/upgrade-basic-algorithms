
const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

/* 1.1*/

console.log(avengers[0]);

/*1.2*/

avengers[0] = "IRON MAN";
console.log(avengers);

/* 1.3*/

console.log(avengers.length);

/* 1.4*/

const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];

rickAndMortyCharacters.unshift('Morty', 'Summer');

console.log(rickAndMortyCharacters);
console.log(rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

/* 1.5*/

const rickAndMortyCharacterss = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];

rickAndMortyCharacterss.pop();

console.log(rickAndMortyCharacterss[0],rickAndMortyCharacterss[rickAndMortyCharacterss.length - 1]);

/* 1.6*/

const rickAndMortyCharactersss = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];

rickAndMortyCharactersss.splice(1,1);

console.log(rickAndMortyCharactersss);